﻿namespace Qort_UI_Wrapper
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PassManagerSytle = new MetroFramework.Components.MetroStyleManager(this.components);
            this.In_AddPassword = new MetroFramework.Controls.MetroTile();
            this.In_GetPassword = new MetroFramework.Controls.MetroTile();
            ((System.ComponentModel.ISupportInitialize)(this.PassManagerSytle)).BeginInit();
            this.SuspendLayout();
            // 
            // PassManagerSytle
            // 
            this.PassManagerSytle.Owner = this;
            this.PassManagerSytle.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // In_AddPassword
            // 
            this.In_AddPassword.ActiveControl = null;
            this.In_AddPassword.Location = new System.Drawing.Point(23, 63);
            this.In_AddPassword.Name = "In_AddPassword";
            this.In_AddPassword.Size = new System.Drawing.Size(118, 111);
            this.In_AddPassword.TabIndex = 0;
            this.In_AddPassword.Text = "Add Password";
            this.In_AddPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.In_AddPassword.UseSelectable = true;
            this.In_AddPassword.Click += new System.EventHandler(this.In_AddPassword_Click);
            // 
            // In_GetPassword
            // 
            this.In_GetPassword.ActiveControl = null;
            this.In_GetPassword.Location = new System.Drawing.Point(147, 63);
            this.In_GetPassword.Name = "In_GetPassword";
            this.In_GetPassword.Size = new System.Drawing.Size(118, 111);
            this.In_GetPassword.TabIndex = 1;
            this.In_GetPassword.Text = "Get Password";
            this.In_GetPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.In_GetPassword.UseSelectable = true;
            this.In_GetPassword.Click += new System.EventHandler(this.In_GetPassword_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(285, 198);
            this.Controls.Add(this.In_GetPassword);
            this.Controls.Add(this.In_AddPassword);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Password Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PassManagerSytle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Components.MetroStyleManager PassManagerSytle;
        private MetroFramework.Controls.MetroTile In_GetPassword;
        private MetroFramework.Controls.MetroTile In_AddPassword;
    }
}