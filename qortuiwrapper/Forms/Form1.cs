﻿using CefSharp;
using CefSharp.WinForms;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;

namespace Qort_UI_Wrapper
{
    public partial class Main : MetroFramework.Forms.MetroForm
    {
        private string CurrentDirectory = Directory.GetCurrentDirectory().ToString();

        private string PasswordFileDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Passwords");

        private Form2 PassManager = new Form2();

        private Process NodeProc = new Process();

        private JArray Addresses;

        public ChromiumWebBrowser chromeBrowser;

        public Main()
        {
            InitializeComponent();
        }

        public void InitializeChromium()
        {
            CefSettings settings = new CefSettings();

            string path = Path.Combine(Environment.CurrentDirectory, "browserCache");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            settings.CachePath = path;

            settings.PersistUserPreferences = true;
            settings.PersistSessionCookies = true;

            Cef.EnableHighDPISupport();

            // Initialize cef with the provided settings
            Cef.Initialize(settings);

            chromeBrowser = new ChromiumWebBrowser("http://127.0.0.1:12388/");

            chromeBrowser.DownloadHandler = new CefDownloadHandler();
            // Add it to the form and fill it to the form window.
            this.BrowserPanel.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //start new console
            //AllocConsole();

            InitializeChromium();

            this.MinimumSize = new System.Drawing.Size(1280, 720);

            Directory.CreateDirectory(PasswordFileDirectory);

            this.StyleManager = MSMMain;
            /*
            if (File.Exists(Path.Combine(CurrentDirectory, ".bat")))
            {
                //change this bit to run the ui server
                DialogResult dialogResult = MetroFramework.MetroMessageBox.Show(this, "Would You like to run a local wallet?", "Start Local Wallet?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialogResult == DialogResult.Yes)
                {
                    NodeProc.StartInfo.FileName = Path.Combine(CurrentDirectory, "run.bat");
                    NodeProc.StartInfo.UseShellExecute = false;

                    NodeProc.Start();
                }
            }
            */

            if (File.Exists(Path.Combine(PasswordFileDirectory, "Servers.json")) != true)
            {
                JArray jar = new JArray();

                jar.Add("Server ip here");

                File.AppendAllText(Path.Combine(PasswordFileDirectory, "Servers.json"), jar.ToString());
            }

            Addresses = ReadServerJson();
        }

        private JArray ReadServerJson()
        {
            var pathToJson = Path.Combine(PasswordFileDirectory, "Servers.json");
            var r = new StreamReader(pathToJson);
            var myJson = r.ReadToEnd();

            if (myJson != "")
            {
                return JArray.Parse(myJson);
            }
            else
            {
                return null;
            }
        }

        public static bool CheckForNodeConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://127.0.0.1:12388/"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool CheckForNodeConnection(string URL)
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead(URL))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private string GetFastestServer()
        {
            string fastest = "";
            long fastestPing = 100000000;

            foreach (string address in Addresses)
            {
                if (CheckForNodeConnection(address.ToString()))
                {
                    long ping = PingServer(address);
                    if (ping > fastestPing)
                    {
                        fastest = address;
                        fastestPing = ping;
                    }
                }
            }
            return fastest;
        }

        private void PassManager_Tile_Click(object sender, EventArgs e)
        {
            if (PassManager.IsDisposed != true)
            {
                //PassManager.Show();
                PassManager.ShowDialog();
            }
            else
            {
                PassManager = new Form2();
                //PassManager.Show();
                PassManager.ShowDialog();
            }
            PassManager.BringToFront();
        }

        private long PingServer(string url)
        {
            Uri uri = new Uri(url);
            Ping myPing = new Ping();
            PingReply reply = myPing.Send(uri.Host);
            if (reply != null)
            {
                return reply.RoundtripTime;
            }
            return 100000000000;
        }

        private void InstallDependencies()
        {
            Invoke(new MethodInvoker(() => btn_installDepends.Enabled = false));
            //assumes chocolatey is installed if exe is found
            if (!File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "chocolatey\\choco.exe")))
            {
                string command = @"powershell -NoProfile -ExecutionPolicy Bypass -Command ""iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))"" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin";

                //else install chocolatey
                System.Diagnostics.ProcessStartInfo procStartInfo =
                new System.Diagnostics.ProcessStartInfo("cmd.exe", $"/c {command}");

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;

                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = false;

                procStartInfo.Verb = "runas";

                Process chocoInstallProcess = new Process();

                chocoInstallProcess.StartInfo = procStartInfo;

                chocoInstallProcess.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);

                chocoInstallProcess.Start();
                chocoInstallProcess.BeginOutputReadLine();
                chocoInstallProcess.WaitForExit();
            }

            List<string> chocoCommands = new List<string>
            {
                "git",
                "nodejs.install",
                "Yarn"
            };

            //run choco commands

            foreach (string command in chocoCommands)
            {
                Console.WriteLine($"Installing {command}");
                System.Diagnostics.ProcessStartInfo procStartInfo =
                new System.Diagnostics.ProcessStartInfo("cmd.exe", $"/c choco install {command} -fy");

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;

                procStartInfo.UseShellExecute = false;

                procStartInfo.Verb = "runas";

                procStartInfo.WindowStyle = ProcessWindowStyle.Maximized;

                Process chocoInstallProcess = new Process();

                chocoInstallProcess.StartInfo = procStartInfo;

                chocoInstallProcess.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);

                chocoInstallProcess.Start();
                chocoInstallProcess.BeginOutputReadLine();
                chocoInstallProcess.WaitForExit();
            }
            Invoke(new MethodInvoker(() => btn_installDepends.Enabled = true));
        }

        public static void DeleteDirectory(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(target_dir, false);
        }

        public void InstallUI()
        {
            Invoke(new MethodInvoker(() => btn_InstallUI.Enabled = false));
            List<string> repos = new List<string>()
            {
                "https://github.com/QORT/qortal-ui",
                "https://github.com/QORT/frag-default-plugins",
                "https://github.com/QORT/frag-core",
                "https://github.com/QORT/frag-qortal-crypto"
            };

            string uiPath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "QORT\\UI");

            Console.WriteLine(uiPath);

            if (!Directory.Exists(uiPath))
            {
                Directory.CreateDirectory(uiPath);
            }
            else
            {
                Console.WriteLine("Resetting UI install...");
                //delete ui
                foreach (string repo in repos)
                {
                    int lastindex = repo.LastIndexOf("/");
                    string foldername = repo.Substring(lastindex + 1, repo.Length - lastindex - 1);

                    Console.WriteLine($"trying to delete {foldername}");

                    if (Directory.Exists(Path.Combine(uiPath, foldername)))
                    {
                        DeleteDirectory(Path.Combine(uiPath, foldername));
                    }
                }
            }

            Console.WriteLine("Cloning UI repos...");
            //clone repos
            foreach (string repo in repos)
            {
                int lastindex = repo.LastIndexOf("/");
                string foldername = repo.Substring(lastindex + 1, repo.Length - lastindex - 1);

                Console.WriteLine(Path.Combine(uiPath, foldername));

                if (!Directory.Exists(Path.Combine(uiPath, foldername)))
                {
                    Directory.CreateDirectory(Path.Combine(uiPath, foldername));
                }

                ProcessStartInfo pinf = new ProcessStartInfo("cmd.exe", $"/c git clone {repo}");
                pinf.RedirectStandardOutput = true;
                pinf.UseShellExecute = false;
                pinf.CreateNoWindow = false;
                pinf.WorkingDirectory = uiPath;
                pinf.Verb = "runas";

                Process p = new Process();

                p.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);

                p.StartInfo = pinf;
                p.Start();
                p.BeginOutputReadLine();
                p.WaitForExit();
            }

            Console.WriteLine("Installing and linking Yarns");
            //install and link yarn
            foreach (string repo in repos)
            {
                if (repo == repos[0]) continue; //if repo is qortal ui

                int lastindex = repo.LastIndexOf("/");
                string foldername = repo.Substring(lastindex + 1, repo.Length - lastindex - 1);
                List<string> yarnCommands = new List<string>()
                {
                    "yarn install",
                    "yarn unlink",
                    "yarn link"
                };
                foreach (string com in yarnCommands)
                {
                    ProcessStartInfo pinf = new ProcessStartInfo("cmd.exe", $"/c @powershell -NoProfile -ExecutionPolicy Bypass -Command \"{com}\"");
                    pinf.RedirectStandardOutput = true;
                    pinf.UseShellExecute = false;
                    pinf.CreateNoWindow = false;
                    pinf.Verb = "runas";
                    pinf.WorkingDirectory = Path.Combine(uiPath, foldername);

                    Process p = new Process();
                    p.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);

                    p.StartInfo = pinf;
                    p.Start();
                    p.BeginOutputReadLine();
                    p.WaitForExit();
                }
            }

            List<string> linkNames = new List<string>
            {
                "@frag-crypto/frag-core",
                "@frag-crypto/frag-default-plugins",
                "@frag-crypto/frag-qortal-crypto"
            };

            Console.WriteLine("linking yarns");
            //build yarnBall
            foreach (string linkname in linkNames)
            {
                Console.WriteLine($"linking yarn {linkname}");
                ProcessStartInfo pinf = new ProcessStartInfo("cmd.exe", $"/c @powershell -NoProfile -ExecutionPolicy Bypass -Command \"yarn link {linkname}\"");
                pinf.RedirectStandardOutput = true;
                pinf.UseShellExecute = false;

                pinf.Verb = "runas";
                pinf.WorkingDirectory = Path.Combine(uiPath, "qortal-ui");

                Process p = new Process();
                p.StartInfo = pinf;
                p.Start();
                p.BeginOutputReadLine();
                p.WaitForExit();
            }

            Console.WriteLine("building yarnball");
            ProcessStartInfo pinf1 = new ProcessStartInfo("cmd.exe", "/c @powershell -NoProfile -ExecutionPolicy Bypass -Command \"yarn run build\"");
            pinf1.RedirectStandardOutput = true;
            pinf1.UseShellExecute = false;

            pinf1.Verb = "runas";
            pinf1.WorkingDirectory = Path.Combine(uiPath, "qortal-ui");

            Process p1 = new Process();
            p1.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);
            p1.StartInfo = pinf1;
            p1.Start();
            p1.BeginOutputReadLine();
            p1.WaitForExit();
            Console.WriteLine("Done");
            Invoke(new MethodInvoker(() => btn_InstallUI.Enabled = true));
        }

        private Process UIProcess = null;

        private async Task StartUIAsync()
        {
            Invoke(new MethodInvoker(() => Btn_RunUI.Text = "Stop UI"));
            Invoke(new MethodInvoker(() => Btn_RunUI.Enabled = true));

            string uiPath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "QORT\\UI");

            ProcessStartInfo pinf1 = new ProcessStartInfo("cmd.exe", "/c yarn run server");
            pinf1.RedirectStandardOutput = false;
            pinf1.UseShellExecute = true;

            pinf1.Verb = "runas";
            pinf1.WorkingDirectory = Path.Combine(uiPath, "qortal-ui");

            UIProcess = new Process();
            UIProcess.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);
            UIProcess.StartInfo = pinf1;

            UIProcess.EnableRaisingEvents = true;
            UIProcess.Exited += P_Exited;
            UIProcess.Start();

            UIProcess.WaitForExit();

            await Task.Delay(1500);

            return;
        }

        private void P_Exited(object sender, EventArgs e)
        {
            try
            {
                Invoke(new MethodInvoker(() => Btn_RunUI.Text = "Run UI"));
                Invoke(new MethodInvoker(() => Btn_RunUI.Enabled = true));
                Invoke(new MethodInvoker(() => ServerToggle_Toggle.Checked = false));

                UIProcess = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (UIProcess != null)
            {
                Invoke(new MethodInvoker(() => Btn_RunUI.Text = "Run UI"));
                Invoke(new MethodInvoker(() => Btn_RunUI.Enabled = true));
                Invoke(new MethodInvoker(() => ServerToggle_Toggle.Checked = false));

                KillProcessAndChildren(UIProcess.Id);
            }
        }

        private void ServerToggle_Toggle_CheckedChanged(object sender, EventArgs e)
        {
            return;
            if (ServerToggle_Toggle.Checked == true)
            {
                if (CheckForNodeConnection() == true)
                {
                    string Local = "http://127.0.0.1:12388/";
                    chromeBrowser.Load(Local);
                }
                else
                {
                    MetroFramework.MetroMessageBox.Show(this, "Could Not Find Node Running On LOCALHOST", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ServerToggle_Toggle.Checked = false;
                }
            }
            else
            {
                chromeBrowser.Load("http://127.0.0.1:12388/");
            }
        }

        private void btn_installDepends_Click(object sender, EventArgs e)
        {
            try
            {
                Task.Run(() => InstallDependencies());
            }
            catch (Exception except)
            {
                btn_installDepends.Enabled = true;
                MetroFramework.MetroMessageBox.Show(this, $"Failed {except}", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_InstallUI_Click(object sender, EventArgs e)
        {
            try
            {
                Task.Run(() => InstallUI());
            }
            catch (Exception except)
            {
                btn_InstallUI.Enabled = true;
                MetroFramework.MetroMessageBox.Show(this, $"Failed {except}", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_RunUI_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_RunUI.Text == "Run UI")
                {
                    Task.Run(() => StartUIAsync());
                }
                else
                {
                    if (UIProcess != null)
                    {
                        Invoke(new MethodInvoker(() => Btn_RunUI.Text = "Run UI"));
                        Invoke(new MethodInvoker(() => Btn_RunUI.Enabled = true));
                        Invoke(new MethodInvoker(() => ServerToggle_Toggle.Checked = false));

                        KillProcessAndChildren(UIProcess.Id);
                    }
                }
            }
            catch (Exception except)
            {
                Btn_RunUI.Enabled = true;
                MetroFramework.MetroMessageBox.Show(this, $"Failed {except}", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static void KillProcessAndChildren(int pid)
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher
                ("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                proc.Kill();
            }
            catch (ArgumentException)
            {
                // Process already exited.
            }
        }

        private Timer buttonResetTimer = null;

        private void metroTile1_Click(object sender, EventArgs e)
        {
            try
            {
                chromeBrowser.Load("http://127.0.0.1:12388/");
                metroTile1.Enabled = false;

                buttonResetTimer = new Timer();
                buttonResetTimer.Interval = 1500;
                buttonResetTimer.Tick += ButtonResetTimer_Tick;
                buttonResetTimer.Start();
            }
            catch (Exception except)
            {
                Console.WriteLine(except);
                metroTile1.Enabled = true;
                return;
            }
        }

        private void ButtonResetTimer_Tick(object sender, EventArgs e)
        {
            metroTile1.Enabled = true;
            buttonResetTimer.Stop();
            buttonResetTimer.Dispose();
            buttonResetTimer = null;
        }
    }
}