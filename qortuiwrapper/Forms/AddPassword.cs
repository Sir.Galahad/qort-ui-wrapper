﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Windows.Forms;

namespace Qort_UI_Wrapper
{
    public partial class AddPassword : MetroFramework.Forms.MetroForm
    {
        private string CurrentDirectory = Directory.GetCurrentDirectory().ToString();

        private string PasswordFileDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Passwords");

        private JObject Passwordjson;

        private JObject SelectedJson;

        //PinkHemalayanSaltRCP3xndzuUdYsBphLAUMfqzQXg825KSGPeJktaskPKmB83BvkLQqEuvDzEz3ZATtq6qAnttsEm8W56gE2ZQbLCZscg3CvntZz95AU3Uz4UtyKQCDSMVfUBCEth2v5W6NmeekpRKRDvyV8jUEdgQ6HKy6ggeEERNBYnLkm2NS43F82VPemnvV3Ra7HUZcxtpYVaSuPyaCZT3nnbb65JxHUPLHwyvfGT3wGTSUJ9Rw5NLwY4yDqn8narSUeLRbwzwX

        public AddPassword()
        {
            InitializeComponent();
        }

        private void AddPassword_Load(object sender, EventArgs e)
        {
            this.StyleManager = Addpasswordstyle;

            Directory.CreateDirectory(PasswordFileDirectory);
            if (File.Exists(Path.Combine(PasswordFileDirectory, "Passwords.json")) == false)
            {
                File.AppendAllText(Path.Combine(PasswordFileDirectory, "Passwords.json"), "{}");
            }
            this.BringToFront();
            this.Focus();

            Passwordjson = ReadPasswordJson();
        }

        private void In_PasswordPin_TextChanged(object sender, EventArgs e)
        {
            if (In_UnencryptedPassword.Text != "")
            {
                try
                {
                    Out_EncryptedPassword.Text = Encryption.EncryptString(In_UnencryptedPassword.Text, In_PasswordPin.Text).ToString();
                }
                catch { }
            }
        }

        private JObject ReadPasswordJson()
        {
            var pathToJson = Path.Combine(PasswordFileDirectory, "Passwords.json");
            var r = new StreamReader(pathToJson);
            var myJson = r.ReadToEnd();

            if (myJson != "")
            {
                return JObject.Parse(myJson);
            }
            else
            {
                return null;
            }
        }

        private void In_UnencryptedPassword_TextChanged(object sender, EventArgs e)
        {
            if (In_UnencryptedPassword.Text != "")
            {
                try
                {
                    Out_EncryptedPassword.Text = Encryption.EncryptString(In_UnencryptedPassword.Text, In_PasswordPin.Text).ToString();
                }
                catch { }
            }
        }

        private void In_EncryptAccept_Click(object sender, EventArgs e)
        {
            JObject tempJob = new JObject();

            if (In_PasswordLabel.Text != "")
            {
                if (Passwordjson.ContainsKey(In_PasswordLabel.Text))
                {
                    return;
                }

                tempJob.Add(new JProperty("pin", Encryption.EncryptString(In_PasswordPin.Text, In_PasswordPin.Text)));
                tempJob.Add(new JProperty("Passphrase", Encryption.EncryptString(In_UnencryptedPassword.Text, In_PasswordPin.Text)));

                Passwordjson.Add(new JProperty(In_PasswordLabel.Text, tempJob));
            }

            File.WriteAllText(Path.Combine(PasswordFileDirectory, "Password Recovery " + DateTimeOffset.Now.ToUnixTimeSeconds().ToString() + ".json"), Passwordjson.ToString());
            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.Delete(Path.Combine(PasswordFileDirectory, "Passwords.json"));
            File.WriteAllText(Path.Combine(PasswordFileDirectory, "Passwords.json"), Passwordjson.ToString());

            MetroFramework.MetroMessageBox.Show(this, "Passphrase Encrypted!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Question);
            this.Close();
        }
    }
}