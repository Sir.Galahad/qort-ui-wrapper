﻿namespace Qort_UI_Wrapper
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.MSMMain = new MetroFramework.Components.MetroStyleManager(this.components);
            this.ServerToggle_Toggle = new MetroFramework.Controls.MetroToggle();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.BrowserPanel = new System.Windows.Forms.Panel();
            this.btn_InstallUI = new MetroFramework.Controls.MetroTile();
            this.btn_installDepends = new MetroFramework.Controls.MetroTile();
            this.PassManager_Tile = new MetroFramework.Controls.MetroTile();
            this.Btn_RunUI = new MetroFramework.Controls.MetroTile();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            ((System.ComponentModel.ISupportInitialize)(this.MSMMain)).BeginInit();
            this.SuspendLayout();
            // 
            // MSMMain
            // 
            this.MSMMain.Owner = this;
            this.MSMMain.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // ServerToggle_Toggle
            // 
            resources.ApplyResources(this.ServerToggle_Toggle, "ServerToggle_Toggle");
            this.ServerToggle_Toggle.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.ServerToggle_Toggle.FontWeight = MetroFramework.MetroLinkWeight.Bold;
            this.ServerToggle_Toggle.Name = "ServerToggle_Toggle";
            this.ServerToggle_Toggle.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ServerToggle_Toggle.UseSelectable = true;
            this.ServerToggle_Toggle.Click += new System.EventHandler(this.ServerToggle_Toggle_CheckedChanged);
            // 
            // metroLabel2
            // 
            resources.ApplyResources(this.metroLabel2, "metroLabel2");
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.Name = "metroLabel2";
            // 
            // BrowserPanel
            // 
            resources.ApplyResources(this.BrowserPanel, "BrowserPanel");
            this.BrowserPanel.Name = "BrowserPanel";
            // 
            // btn_InstallUI
            // 
            this.btn_InstallUI.ActiveControl = null;
            resources.ApplyResources(this.btn_InstallUI, "btn_InstallUI");
            this.btn_InstallUI.Name = "btn_InstallUI";
            this.btn_InstallUI.UseSelectable = true;
            this.btn_InstallUI.Click += new System.EventHandler(this.btn_InstallUI_Click);
            // 
            // btn_installDepends
            // 
            this.btn_installDepends.ActiveControl = null;
            resources.ApplyResources(this.btn_installDepends, "btn_installDepends");
            this.btn_installDepends.Name = "btn_installDepends";
            this.btn_installDepends.UseSelectable = true;
            this.btn_installDepends.Click += new System.EventHandler(this.btn_installDepends_Click);
            // 
            // PassManager_Tile
            // 
            this.PassManager_Tile.ActiveControl = null;
            resources.ApplyResources(this.PassManager_Tile, "PassManager_Tile");
            this.PassManager_Tile.Name = "PassManager_Tile";
            this.PassManager_Tile.UseSelectable = true;
            this.PassManager_Tile.Click += new System.EventHandler(this.PassManager_Tile_Click);
            // 
            // Btn_RunUI
            // 
            this.Btn_RunUI.ActiveControl = null;
            resources.ApplyResources(this.Btn_RunUI, "Btn_RunUI");
            this.Btn_RunUI.Name = "Btn_RunUI";
            this.Btn_RunUI.UseSelectable = true;
            this.Btn_RunUI.Click += new System.EventHandler(this.Btn_RunUI_Click);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            resources.ApplyResources(this.metroTile1, "metroTile1");
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.UseSelectable = true;
            this.metroTile1.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // Main
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.Controls.Add(this.PassManager_Tile);
            this.Controls.Add(this.metroTile1);
            this.Controls.Add(this.btn_installDepends);
            this.Controls.Add(this.Btn_RunUI);
            this.Controls.Add(this.btn_InstallUI);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.ServerToggle_Toggle);
            this.Controls.Add(this.BrowserPanel);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "Main";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MSMMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public MetroFramework.Components.MetroStyleManager MSMMain;
        private MetroFramework.Controls.MetroToggle ServerToggle_Toggle;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.Panel BrowserPanel;
        private MetroFramework.Controls.MetroTile btn_installDepends;
        private MetroFramework.Controls.MetroTile btn_InstallUI;
        private MetroFramework.Controls.MetroTile PassManager_Tile;
        private MetroFramework.Controls.MetroTile metroTile1;
        private MetroFramework.Controls.MetroTile Btn_RunUI;
    }
}

