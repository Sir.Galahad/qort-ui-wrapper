﻿using System;
using System.Windows.Forms;

namespace Qort_UI_Wrapper
{
    public partial class Form2 : MetroFramework.Forms.MetroForm
    {
        private AddPassword AddPass = new AddPassword();
        private DecryptPassword GetPass = new DecryptPassword();

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.StyleManager = PassManagerSytle;
            this.BringToFront();
            this.Focus();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void In_AddPassword_Click(object sender, EventArgs e)
        {
            if (AddPass.IsDisposed != true)
            {
                AddPass.ShowDialog();
            }
            else
            {
                AddPass = new AddPassword();
                AddPass.ShowDialog();
            }
            this.Close();
        }

        private void In_GetPassword_Click(object sender, EventArgs e)
        {
            if (GetPass.IsDisposed != true)
            {
                GetPass.ShowDialog();
            }
            else
            {
                GetPass = new DecryptPassword();
                GetPass.ShowDialog();
            }
            this.Close();
        }
    }
}