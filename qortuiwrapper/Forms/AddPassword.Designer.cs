﻿namespace Qort_UI_Wrapper
{
    partial class AddPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.In_UnencryptedPassword = new MetroFramework.Controls.MetroTextBox();
            this.Label_ATMPASSWORD = new MetroFramework.Controls.MetroLabel();
            this.In_PasswordLabel = new MetroFramework.Controls.MetroTextBox();
            this.Label_PasswordPin = new MetroFramework.Controls.MetroLabel();
            this.In_PasswordPin = new MetroFramework.Controls.MetroTextBox();
            this.Out_HashedPassphrase = new MetroFramework.Controls.MetroLabel();
            this.Addpasswordstyle = new MetroFramework.Components.MetroStyleManager(this.components);
            this.Out_EncryptedPassword = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.In_EncryptAccept = new MetroFramework.Controls.MetroTile();
            ((System.ComponentModel.ISupportInitialize)(this.Addpasswordstyle)).BeginInit();
            this.SuspendLayout();
            // 
            // In_UnencryptedPassword
            // 
            // 
            // 
            // 
            this.In_UnencryptedPassword.CustomButton.Image = null;
            this.In_UnencryptedPassword.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.In_UnencryptedPassword.CustomButton.Name = "";
            this.In_UnencryptedPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.In_UnencryptedPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.In_UnencryptedPassword.CustomButton.TabIndex = 1;
            this.In_UnencryptedPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.In_UnencryptedPassword.CustomButton.UseSelectable = true;
            this.In_UnencryptedPassword.CustomButton.Visible = false;
            this.In_UnencryptedPassword.Lines = new string[0];
            this.In_UnencryptedPassword.Location = new System.Drawing.Point(142, 82);
            this.In_UnencryptedPassword.MaxLength = 32767;
            this.In_UnencryptedPassword.Name = "In_UnencryptedPassword";
            this.In_UnencryptedPassword.PasswordChar = '\0';
            this.In_UnencryptedPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.In_UnencryptedPassword.SelectedText = "";
            this.In_UnencryptedPassword.SelectionLength = 0;
            this.In_UnencryptedPassword.SelectionStart = 0;
            this.In_UnencryptedPassword.ShortcutsEnabled = true;
            this.In_UnencryptedPassword.Size = new System.Drawing.Size(158, 23);
            this.In_UnencryptedPassword.TabIndex = 1;
            this.In_UnencryptedPassword.UseSelectable = true;
            this.In_UnencryptedPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.In_UnencryptedPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.In_UnencryptedPassword.TextChanged += new System.EventHandler(this.In_UnencryptedPassword_TextChanged);
            // 
            // Label_ATMPASSWORD
            // 
            this.Label_ATMPASSWORD.AutoSize = true;
            this.Label_ATMPASSWORD.Location = new System.Drawing.Point(142, 60);
            this.Label_ATMPASSWORD.Name = "Label_ATMPASSWORD";
            this.Label_ATMPASSWORD.Size = new System.Drawing.Size(71, 19);
            this.Label_ATMPASSWORD.TabIndex = 1;
            this.Label_ATMPASSWORD.Text = "Secret Text";
            // 
            // In_PasswordLabel
            // 
            // 
            // 
            // 
            this.In_PasswordLabel.CustomButton.Image = null;
            this.In_PasswordLabel.CustomButton.Location = new System.Drawing.Point(91, 1);
            this.In_PasswordLabel.CustomButton.Name = "";
            this.In_PasswordLabel.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.In_PasswordLabel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.In_PasswordLabel.CustomButton.TabIndex = 1;
            this.In_PasswordLabel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.In_PasswordLabel.CustomButton.UseSelectable = true;
            this.In_PasswordLabel.CustomButton.Visible = false;
            this.In_PasswordLabel.Lines = new string[0];
            this.In_PasswordLabel.Location = new System.Drawing.Point(23, 82);
            this.In_PasswordLabel.MaxLength = 32767;
            this.In_PasswordLabel.Name = "In_PasswordLabel";
            this.In_PasswordLabel.PasswordChar = '\0';
            this.In_PasswordLabel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.In_PasswordLabel.SelectedText = "";
            this.In_PasswordLabel.SelectionLength = 0;
            this.In_PasswordLabel.SelectionStart = 0;
            this.In_PasswordLabel.ShortcutsEnabled = true;
            this.In_PasswordLabel.Size = new System.Drawing.Size(113, 23);
            this.In_PasswordLabel.TabIndex = 0;
            this.In_PasswordLabel.UseSelectable = true;
            this.In_PasswordLabel.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.In_PasswordLabel.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Label_PasswordPin
            // 
            this.Label_PasswordPin.AutoSize = true;
            this.Label_PasswordPin.Location = new System.Drawing.Point(306, 60);
            this.Label_PasswordPin.Name = "Label_PasswordPin";
            this.Label_PasswordPin.Size = new System.Drawing.Size(95, 19);
            this.Label_PasswordPin.TabIndex = 5;
            this.Label_PasswordPin.Text = "Encryption Key";
            // 
            // In_PasswordPin
            // 
            // 
            // 
            // 
            this.In_PasswordPin.CustomButton.Image = null;
            this.In_PasswordPin.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.In_PasswordPin.CustomButton.Name = "";
            this.In_PasswordPin.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.In_PasswordPin.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.In_PasswordPin.CustomButton.TabIndex = 1;
            this.In_PasswordPin.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.In_PasswordPin.CustomButton.UseSelectable = true;
            this.In_PasswordPin.CustomButton.Visible = false;
            this.In_PasswordPin.Lines = new string[0];
            this.In_PasswordPin.Location = new System.Drawing.Point(306, 82);
            this.In_PasswordPin.MaxLength = 32767;
            this.In_PasswordPin.Name = "In_PasswordPin";
            this.In_PasswordPin.PasswordChar = '\0';
            this.In_PasswordPin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.In_PasswordPin.SelectedText = "";
            this.In_PasswordPin.SelectionLength = 0;
            this.In_PasswordPin.SelectionStart = 0;
            this.In_PasswordPin.ShortcutsEnabled = true;
            this.In_PasswordPin.Size = new System.Drawing.Size(158, 23);
            this.In_PasswordPin.TabIndex = 3;
            this.In_PasswordPin.UseSelectable = true;
            this.In_PasswordPin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.In_PasswordPin.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.In_PasswordPin.TextChanged += new System.EventHandler(this.In_PasswordPin_TextChanged);
            // 
            // Out_HashedPassphrase
            // 
            this.Out_HashedPassphrase.AutoSize = true;
            this.Out_HashedPassphrase.Location = new System.Drawing.Point(23, 108);
            this.Out_HashedPassphrase.Name = "Out_HashedPassphrase";
            this.Out_HashedPassphrase.Size = new System.Drawing.Size(108, 19);
            this.Out_HashedPassphrase.TabIndex = 6;
            this.Out_HashedPassphrase.Text = "Encrypted Secret";
            this.Out_HashedPassphrase.WrapToLine = true;
            // 
            // Addpasswordstyle
            // 
            this.Addpasswordstyle.Owner = this;
            this.Addpasswordstyle.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // Out_EncryptedPassword
            // 
            // 
            // 
            // 
            this.Out_EncryptedPassword.CustomButton.Image = null;
            this.Out_EncryptedPassword.CustomButton.Location = new System.Drawing.Point(419, 1);
            this.Out_EncryptedPassword.CustomButton.Name = "";
            this.Out_EncryptedPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.Out_EncryptedPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.Out_EncryptedPassword.CustomButton.TabIndex = 1;
            this.Out_EncryptedPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Out_EncryptedPassword.CustomButton.UseSelectable = true;
            this.Out_EncryptedPassword.CustomButton.Visible = false;
            this.Out_EncryptedPassword.Lines = new string[0];
            this.Out_EncryptedPassword.Location = new System.Drawing.Point(23, 130);
            this.Out_EncryptedPassword.MaxLength = 32767;
            this.Out_EncryptedPassword.Name = "Out_EncryptedPassword";
            this.Out_EncryptedPassword.PasswordChar = '\0';
            this.Out_EncryptedPassword.ReadOnly = true;
            this.Out_EncryptedPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Out_EncryptedPassword.SelectedText = "";
            this.Out_EncryptedPassword.SelectionLength = 0;
            this.Out_EncryptedPassword.SelectionStart = 0;
            this.Out_EncryptedPassword.ShortcutsEnabled = true;
            this.Out_EncryptedPassword.Size = new System.Drawing.Size(441, 23);
            this.Out_EncryptedPassword.TabIndex = 7;
            this.Out_EncryptedPassword.UseSelectable = true;
            this.Out_EncryptedPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.Out_EncryptedPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 60);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(80, 19);
            this.metroLabel1.TabIndex = 8;
            this.metroLabel1.Text = "Secret Label";
            // 
            // In_EncryptAccept
            // 
            this.In_EncryptAccept.ActiveControl = null;
            this.In_EncryptAccept.Location = new System.Drawing.Point(470, 60);
            this.In_EncryptAccept.Name = "In_EncryptAccept";
            this.In_EncryptAccept.Size = new System.Drawing.Size(100, 93);
            this.In_EncryptAccept.TabIndex = 4;
            this.In_EncryptAccept.Text = "Encrypt";
            this.In_EncryptAccept.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.In_EncryptAccept.UseSelectable = true;
            this.In_EncryptAccept.Click += new System.EventHandler(this.In_EncryptAccept_Click);
            // 
            // AddPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 171);
            this.Controls.Add(this.In_EncryptAccept);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.Out_EncryptedPassword);
            this.Controls.Add(this.Out_HashedPassphrase);
            this.Controls.Add(this.Label_PasswordPin);
            this.Controls.Add(this.In_PasswordPin);
            this.Controls.Add(this.In_PasswordLabel);
            this.Controls.Add(this.Label_ATMPASSWORD);
            this.Controls.Add(this.In_UnencryptedPassword);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddPassword";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Encrypt Secret";
            this.Load += new System.EventHandler(this.AddPassword_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Addpasswordstyle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox In_UnencryptedPassword;
        private MetroFramework.Controls.MetroLabel Label_ATMPASSWORD;
        private MetroFramework.Controls.MetroTextBox In_PasswordLabel;
        private MetroFramework.Controls.MetroLabel Label_PasswordPin;
        private MetroFramework.Controls.MetroTextBox In_PasswordPin;
        private MetroFramework.Controls.MetroLabel Out_HashedPassphrase;
        private MetroFramework.Components.MetroStyleManager Addpasswordstyle;
        private MetroFramework.Controls.MetroTextBox Out_EncryptedPassword;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTile In_EncryptAccept;
    }
}