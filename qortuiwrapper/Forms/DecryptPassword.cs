﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Windows.Forms;

namespace Qort_UI_Wrapper
{
    public partial class DecryptPassword : MetroFramework.Forms.MetroForm
    {
        private string CurrentDirectory = Directory.GetCurrentDirectory().ToString();

        private string PasswordFileDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Passwords");

        private JObject Passwordjson;

        private JObject SelectedJson;

        public DecryptPassword()
        {
            InitializeComponent();
        }

        private void DecryptPassword_Load(object sender, EventArgs e)
        {
            this.StyleManager = metroStyleManager1;
            Passwordjson = ReadPasswordJson();
            foreach (var jar in Passwordjson)
            {
                metroComboBox1.Items.Add(jar.Key);
            }
        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (var jar in Passwordjson)
            {
                if (jar.Key == metroComboBox1.Text)
                {
                    SelectedJson = JObject.Parse(jar.Value.ToString());
                }
            }
        }

        private void AddPassword_Load(object sender, EventArgs e)
        {
            this.StyleManager = metroStyleManager1;

            Directory.CreateDirectory(PasswordFileDirectory);
            if (File.Exists(Path.Combine(PasswordFileDirectory, "Passwords.json")) == false)
            {
                File.AppendAllText(Path.Combine(PasswordFileDirectory, "Passwords.json"), "{}");
            }
            this.BringToFront();
            this.Focus();

            Passwordjson = ReadPasswordJson();
        }

        private JObject ReadPasswordJson()
        {
            var pathToJson = Path.Combine(PasswordFileDirectory, "Passwords.json");
            var r = new StreamReader(pathToJson);
            var myJson = r.ReadToEnd();

            if (myJson != "")
            {
                return JObject.Parse(myJson);
            }
            else
            {
                return null;
            }
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            if (metroTextBox1.Text == "")
            {
                return;
            }
            try
            {
                if (Encryption.DecryptString(SelectedJson["pin"].ToString(), metroTextBox1.Text) != metroTextBox1.Text)
                {
                    return;
                }
            }
            catch (Exception)
            {
                MetroFramework.MetroMessageBox.Show(this, "Invalid Decryption Key!", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            System.Windows.Forms.Clipboard.SetText(Encryption.DecryptString(SelectedJson["Passphrase"].ToString(), metroTextBox1.Text));

            MetroFramework.MetroMessageBox.Show(this, "Passphrase Copied to Clipboard!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Question);

            this.Close();
        }
    }
}